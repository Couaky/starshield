﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

    public float speed;
    public int limitX;

    public int destructionPoints;

    public GameObject explosionObject;

    private ScoreManager scoreManager;

    public void SetTarget(Vector3 targetPosition) {
        transform.LookAt(targetPosition);
    }

	// Use this for initialization
	void Start () {
        GameObject managers = GameObject.Find("Managers");
        scoreManager = managers.GetComponent<ScoreManager>();
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x < limitX) {
            Destroy(gameObject);
        } else {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
	}

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Explosion") || other.CompareTag("PlayerShip")) {
            Destroy(gameObject);
            Instantiate(explosionObject, transform.position, Quaternion.identity);

            if (other.CompareTag("Explosion"))
                scoreManager.AddScore(destructionPoints);
        }
    }
}
