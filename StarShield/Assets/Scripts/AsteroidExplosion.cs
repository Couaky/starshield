﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidExplosion : MonoBehaviour {

    public float explosionDuration;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, explosionDuration);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
