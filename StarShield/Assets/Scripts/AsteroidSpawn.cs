﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawn : MonoBehaviour {

    [System.Serializable]
    public class FloatBox {
        public float min;
        public float max;
    }

    [System.Serializable]
    public class IntBox {
        public int min;
        public int max;
    }

    // Area box
    public FloatBox zBox;

    // Target line
    public float targetX;

    // Asteroids to spawn
    public GameObject[] asteroids;

    // Startup setting
    public float startOffsetTime;

    // Waves
    public FloatBox intervalInWave;
    public IntBox asteroidCountPerWave;

    // Sequences
    public FloatBox intervalBetweenWaves;
    public int wavesPerSequence;

    // Level
    public float sequencesInterval;

    IEnumerator ProcessWaves() {
        while (true) {
            // Start sequence -> spawn waves
            for (int waveI = 0; waveI < wavesPerSequence; waveI++) {
                // Start wave -> spawn asteroids packet
                int asteroidsCount = Random.Range(asteroidCountPerWave.min, asteroidCountPerWave.max);
                for (int asteroidI = 0; asteroidI < asteroidsCount; asteroidI++) {
                    Vector3 spawnPos = transform.position;
                    spawnPos.z = Random.Range(zBox.min, zBox.max);
                    Vector3 spawnTarget = transform.position;
                    spawnTarget.x = targetX;
                    spawnTarget.z = Random.Range(zBox.min, zBox.max);

                    GameObject asteroidObj = Instantiate(asteroids[Random.Range(0, asteroids.Length - 1)], spawnPos, Quaternion.identity);
                    asteroidObj.GetComponent<Asteroid>().SetTarget(spawnTarget);

                    yield return new WaitForSeconds(Random.Range(intervalInWave.min, intervalInWave.max));
                }
                yield return new WaitForSeconds(Random.Range(intervalBetweenWaves.min, intervalBetweenWaves.max));
            }
            yield return new WaitForSeconds(sequencesInterval);
        }
    }

    // Use this for initialization
    IEnumerator Start () {
        yield return new WaitForSeconds(startOffsetTime);
        StartCoroutine(ProcessWaves());
    }
	
	// Update is called once per frame
	void Update () {
	}
}
