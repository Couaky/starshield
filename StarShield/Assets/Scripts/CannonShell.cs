﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShell : MonoBehaviour {

    public float speed;
    public GameObject shellExplosion;

    private Vector3 target;

    public void SetTarget(Vector3 targetPosition) {
        target = targetPosition;
        transform.LookAt(new Vector3(targetPosition.x, transform.position.y, targetPosition.z));
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 dirVector = target - transform.position;
        float walk = speed * Time.deltaTime;
        if (dirVector.magnitude <= walk) {
            Destroy(gameObject);
            Detonate(target);
        } else {
            transform.position += dirVector.normalized * walk;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Ennemy") || other.CompareTag("Explosion"))
            Detonate(transform.position);
    }
    
    void Detonate(Vector3 pos) {
        Instantiate(shellExplosion, pos, Quaternion.identity);
        Destroy(gameObject);
    }
}
