﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour {

    public float speed = 25.0f;
    public float fastSpeed = 70.0f;
    public Rect bounds;

    Vector3 targetPosition;

	// Use this for initialization
	void Start () {
        targetPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 move = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
        if (move.magnitude != 0.0f) {
            float currentSpeed = Input.GetButton("FastMove") ? fastSpeed : speed;
            targetPosition += (move.normalized * currentSpeed * Time.deltaTime);
            targetPosition.x = Mathf.Clamp(targetPosition.x, bounds.xMin, bounds.xMax);
            targetPosition.z = Mathf.Clamp(targetPosition.z, bounds.yMin, bounds.yMax);
        }
        transform.position = targetPosition;
	}
}
