﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    public float duration;
    public float endDuration;

    Animator animator;

    IEnumerator DelayedEndOfExplosion() {
        yield return new WaitForSeconds(duration);
        animator.SetBool("endOfExplosion", true);
        Destroy(gameObject, endDuration);
    }

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        StartCoroutine(DelayedEndOfExplosion());
	}
	
	// Update is called once per frame
	void Update () {
	}
}
