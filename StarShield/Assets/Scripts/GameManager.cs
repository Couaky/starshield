﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject[] playerShips;
    public GameObject gameOverGui;
    public Text gameOverScoreText;

    public bool GameIsOver() { return gameIsOver; }

    private bool gameIsOver = false;
    private PauseManager pauseManager;
    private ScoreManager scoreManager;

    // Use this for initialization
    void Start () {
        pauseManager = GetComponent<PauseManager>();
        scoreManager = GetComponent<ScoreManager>();
    }
	
	// Update is called once per frame
	void Update () {
		if (gameIsOver) {
            // Wait to relaunch game
            if (Input.anyKeyDown) {
                pauseManager.pauseWithoutGUI(false);
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        } else {
            // Check ships
            foreach (GameObject ship in playerShips) {
                if (ship.activeSelf)
                    return;
            }
            // All ship destroyed -> game over !
            gameIsOver = true;
            pauseManager.pauseWithoutGUI(true);
            gameOverScoreText.text = "SCORE: " + scoreManager.GetScore();
            gameOverGui.SetActive(true);
        }
	}
}
