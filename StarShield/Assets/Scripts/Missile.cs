﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

    public float initialSpeed;
    public float mainTrust;
    public float secondaryTrust;
    public float rotationSpeed;
    public float rotationDelay;

    public ParticleSystem trustParticles;
    public GameObject missileExplosion;

    bool rotationMode;
    bool trustMode;
    Vector3 target;
    Vector3 speedDirection;

    public void SetTarget(Vector3 targetPosition) {
        target = targetPosition;
    }

    void Detonate(Vector3 pos) {
        Instantiate(missileExplosion, pos, Quaternion.identity);
        Destroy(gameObject);
    }

    void StartRotation() {
        rotationMode = true;
    }

    // Use this for initialization
    void Start () {
        rotationMode = false;
        trustMode = false;
        speedDirection = transform.forward * initialSpeed;
        Invoke("StartRotation", rotationDelay);
    }
	
	// Update is called once per frame
	void Update () {
        if (trustMode) {
            // Rotation
            Vector3 targetDir = target - transform.position;
            float step = rotationSpeed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);

            // Secondary Trust
            Vector3 localSpeed = transform.InverseTransformDirection(speedDirection);
            speedDirection -= transform.right * Mathf.Min(localSpeed.x, secondaryTrust * Time.deltaTime);
            speedDirection -= transform.up * Mathf.Min(localSpeed.y, secondaryTrust * Time.deltaTime);

            // Main Trust
            speedDirection += transform.forward * mainTrust * Time.deltaTime;
        } else if (rotationMode) {
            // Rotation
            Vector3 targetDir = target - transform.position;
            float step = rotationSpeed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);

            if (Vector3.Angle(transform.forward, targetDir) <= Mathf.Epsilon) {
                trustMode = true;
                trustParticles.Play();
            }
        }

        float walk = speedDirection.magnitude * Time.deltaTime;
        if (Vector3.Distance(transform.position, target) <= walk) {
            Detonate(target);
        } else {
            transform.position += speedDirection * Time.deltaTime;
        }

        /*
            if (Time.timeSinceLevelLoad < targetTriggeredTime) {
            transform.position += transform.forward * speed * Time.deltaTime;
        } else {
            float walk = speed * Time.deltaTime;
            if (Vector3.Distance(transform.position, target) <= walk) {
                Detonate(target);
            } else {
                Vector3 targetDir = target - transform.position;
                // The step size is equal to speed times frame time.
                float step = rotationSpeed * Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
                // Move our position a step closer to the target.
                transform.rotation = Quaternion.LookRotation(newDir);

                transform.position += transform.forward * speed * Time.deltaTime;
            }
        }*/
    }

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Ennemy") || other.CompareTag("Explosion"))
            Detonate(transform.position);
    }
}
