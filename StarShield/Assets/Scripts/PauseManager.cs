﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour {

    public GameObject pauseGui;

    bool isPaused = false;

    private GameManager gameManager;

    public bool gameIsPaused() {
        return isPaused;
    }

    void TogglePause() {
        Debug.Log("Pause!");
        Time.timeScale = 1.0f - Time.timeScale;
        isPaused = Time.timeScale == 0.0f;
        pauseGui.SetActive(isPaused);
    }

    public void pauseWithoutGUI(bool pause) {
        Time.timeScale = pause ? 0.0f: 1.0f;
        isPaused = pause;
        pauseGui.SetActive(false);
    }

	// Use this for initialization
	void Start () {
        gameManager = GetComponent<GameManager>();
        // Pause game at startup
        TogglePause();
    }

    // Update is called once per frame
    void Update() {
        if (!gameManager.GameIsOver()) {
            if (Input.GetButtonUp("Pause")) {
                TogglePause();
            }
        }
    }
}
