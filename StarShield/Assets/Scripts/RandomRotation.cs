﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour {

    float rotationSpeed;
    Vector3 rotationAxis;
    float rotationValue;

	// Use this for initialization
	void Start () {
        rotationAxis = Random.onUnitSphere;
        rotationValue = Random.Range(0, 360);
        rotationSpeed = Random.Range(10, 90);
        transform.rotation = Quaternion.AngleAxis(rotationValue, rotationAxis);
    }
	
	// Update is called once per frame
	void Update () {
        rotationValue += rotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.AngleAxis(rotationValue, rotationAxis);
	}
}
