﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadBar : MonoBehaviour {

    public RectTransform progessBar;
    public Image backgroundBar;
    public Image iconBar;

    private float progress = -1.0f;

    public void SetProgress(float newProgress) {
        if (progress != newProgress) {
            progress = Mathf.Clamp(newProgress, 0.0f, 100.0f);
            progessBar.localScale = new Vector2(progress / 100.0f, 1.0f);

            if (progress == 100.0f) {
                iconBar.color = new Color(0.0f, 1.0f, 0.0f);
            } else {
                iconBar.color = new Color(1.0f, 0.65f, 0.0f);
            }
        }
    }

    public void DisableBar() {
        progress = -1.0f;
        progessBar.localScale = new Vector2(0.0f, 1.0f);
        backgroundBar.color = new Color(0.5f, 0.5f, 0.5f);
        iconBar.color = new Color(0.5f, 0.5f, 0.5f);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}
}
