﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    int currentScore = 0;

    public Text scoreText;

    public int GetScore() {
        return currentScore;
    }

    public void AddScore(int points) {
        currentScore += points;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "Score: " + currentScore;
    }
}
