﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellExplosion : MonoBehaviour {

    public float effectDuration;
    public float animationDuration;

    void DisableEffect() {
        GetComponent<SphereCollider>().enabled = false;
    }

	// Use this for initialization
	void Start () {
        Invoke("DisableEffect", effectDuration);
        Destroy(gameObject, animationDuration);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
