﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ship : MonoBehaviour {

    public GameObject sightCursorObject;
    public GameObject primCanonObject;
    public GameObject secCanonObject;

    public Transform[] cannons;
    public GameObject shellObject;
    public float canonReloadTime;

    public Transform[] missileHoles;
    public GameObject missileObject;
    public float missileReloadTime;
    public ReloadBar[] reloadBars;

    private float[] canonTimeToShot;
    private float[] missileTimeToShot;

    private float hullIntegrity = 100.0f;
    public Text hullText;

    public void ShotWithCannon() {
        if (!gameObject.activeInHierarchy)
            return;

        Debug.Log("Shot cannon to " + sightCursorObject.transform.position);
        // Check first cannon available
        for (int index = 0; index < canonTimeToShot.Length; index++) {
            if (Time.timeSinceLevelLoad >= canonTimeToShot[index]) {
                // Instanciate shell at the cannon
                GameObject shell = Instantiate(shellObject, cannons[index].position, cannons[index].rotation);
                shell.GetComponent<CannonShell>().SetTarget(sightCursorObject.transform.position);
                // Update cannon time shot
                canonTimeToShot[index] = Time.timeSinceLevelLoad + canonReloadTime;
                return;
            }
        }
    }

    public void shotWithMissile() {
        if (!gameObject.activeInHierarchy)
            return;

        Debug.Log("Shot missile to " + sightCursorObject.transform.position);
        // Check first missile available
        for (int index = 0; index < missileTimeToShot.Length; index++) {
            if (Time.timeSinceLevelLoad >= missileTimeToShot[index]) {
                // Instanciate missile at the hole
                GameObject missile = Instantiate(missileObject, missileHoles[index].position, missileHoles[index].rotation);
                missile.GetComponent<Missile>().SetTarget(sightCursorObject.transform.position);
                // Update missile time shot
                missileTimeToShot[index] = Time.timeSinceLevelLoad + missileReloadTime;
                return;
            }
        }
    }

    void RotateToPos(Transform obj, Transform target) {
        /*Vector2 objLook = obj.up;
        Vector2 targetLook = target.position - obj.position;
        float angle = Vector2.SignedAngle(objLook, targetLook);
        obj.Rotate(0.0f, 0.0f, angle);*/

        obj.LookAt(new Vector3(target.position.x, obj.position.y, target.position.z));
    }

    // Use this for initialization
    void Start () {
        if (cannons.Length > 0) {
            Debug.Log("Ship detected " + cannons.Length + " cannons");
            canonTimeToShot = new float[cannons.Length];
            for (int index = 0; index < canonTimeToShot.Length; index++)
                canonTimeToShot[index] = Time.timeSinceLevelLoad;
        }
        if (missileHoles.Length > 0) {
            Debug.Log("Ship detected " + missileHoles.Length + " missiles");
            missileTimeToShot = new float[missileHoles.Length];
            for (int index = 0; index < missileTimeToShot.Length; index++)
                missileTimeToShot[index] = Time.timeSinceLevelLoad;
        }
    }
	
	// Update is called once per frame
	void Update () {
        // Rotate canons to sight if exist
        if (primCanonObject)
            RotateToPos(primCanonObject.transform, sightCursorObject.transform);
        if (secCanonObject)
            RotateToPos(secCanonObject.transform, sightCursorObject.transform);

        // Update reload bars
        float currentTime = Time.timeSinceLevelLoad;
        if (cannons.Length > 0 && cannons.Length == reloadBars.Length) {
            for (int index = 0; index < canonTimeToShot.Length; index++)
                reloadBars[index].SetProgress(GetProgressPercent(canonTimeToShot[index], currentTime, canonReloadTime));
        }
        if (missileHoles.Length > 0 && missileHoles.Length == reloadBars.Length) {
            for (int index = 0; index < missileTimeToShot.Length; index++)
                reloadBars[index].SetProgress(GetProgressPercent(missileTimeToShot[index], currentTime, missileReloadTime));
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Ennemy")) {
            hullIntegrity = Mathf.Max(0.0f, hullIntegrity - 25.0f);
            hullText.text = "HULL: " + hullIntegrity.ToString("0") + "%";
            if (hullIntegrity <= 0.0f) {
                gameObject.SetActive(false);
                hullText.text = "SIGNAL LOST";
                for (int index = 0; index < reloadBars.Length; index++)
                    reloadBars[index].DisableBar();
            }
        }
    }

    float GetProgressPercent(float timeToShoot, float currentTime, float reloadTime) {
        float zeroTime = timeToShoot - reloadTime;
        return ((currentTime - zeroTime) / reloadTime) * 100.0f;
    }
}
