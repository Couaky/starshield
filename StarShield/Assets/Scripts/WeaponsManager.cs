﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsManager : MonoBehaviour {

    public Ship firstShip;
    public Ship mainShip;
    public Ship secondShip;

    PauseManager pauseManager;

	// Use this for initialization
	void Start () {
        pauseManager = GetComponent<PauseManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!pauseManager.gameIsPaused()) {
            if (Input.GetButtonUp("Fire1")) {
                // Try fire missile from first escort
                Debug.Log("Fire1 triggered");
                firstShip.shotWithMissile();
            }
            if (Input.GetButtonUp("Fire2")) {
                // Try fire cannon from main ship
                Debug.Log("Fire2 triggered");
                mainShip.ShotWithCannon();
            }
            if (Input.GetButtonUp("Fire3")) {
                // Try fire missile from second escort
                Debug.Log("Fire3 triggered");
                secondShip.shotWithMissile();
            }
        }
	}
}
